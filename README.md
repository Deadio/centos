# Post-installation setup script for CentOS 7 servers 

(c) Niki Kovacs, 2020

Ce dossier vous fourni un script automatique a lancer après l'installation
de votre serveur CentOS7, vous trouverez également des script d'aide et des 
fichiers de configuration pour les services le plus commun.


## En résumé :

Faites les étapes suivantes.

  1. Installer un système CentOS7 en version minimal.

  2. Créer un utilisateur autre que Root avec des droits administrateur.

  3. Installer Git: `sudo yum install git`

  4. Récuperer le script: `git clone https://gitlab.com/kikinovak/centos-7.git`

  5. Changer le de répertoire: `cd centos-7`

  6. Executer le script: `sudo ./centos-setup.sh --setup`

  7. Buvez un café le temps que le script travail tout seul.

  8. Redémarrer.


## Customiser un serveur CentOS

Transformer une installation CentOS minimale en un serveur fonctionnel demande
une serie d'opération plus ou moins longues. Votre installation peut varier mais 
je le fais d'habitude sur une nouvelle installation de CentOS :


  * Customiser l'invite de commande

  * Customiser l'éditeur de texte VIM.

  * Mise en place des répertoires.

  * Installer un ensemble complet d'outils en ligne de commande.

  * Retirer des paquets inutiles.

  * Authoriser le compte admin a accéder aux log système.

  * Désactiver l'ipv6.
  
  * Configurer un mot de passe sécurisé pour le super user.

  * Etc.

Le  script `centos-setup.sh`réalise toutes les actions.

Configurer Bash et Vim et configurer une meilleur console que celle par défaut :

```
# ./centos-setup.sh --shell
```

Mise en place des répertoires:

```
# ./centos-setup.sh --repos
```

Installer un ensemble complet d'outils en ligne de commande:

```
# ./centos-setup.sh --extra
```

Retirer des paquets inutiles:

```
# ./centos-setup.sh --prune
```

Authoriser le compte admin a accéder aux log système.:

```
# ./centos-setup.sh --logs
```

Désactiver l'ipv6:

```
# ./centos-setup.sh --ipv4
```

Configurer un mot de passe sécurisé pour le super user:

```
# ./centos-setup.sh --sudo
```

Executer toutes les commandes au dessus en 1 fois:

```
# ./centos-setup.sh --setup
```

Retirer les paquets et revenir a la configuration initial:

```
# ./centos-setup.sh --strip
```

Afficher le message d'aide:

```
# ./centos-setup.sh --help
```

Si vous voulez savoir ce qui se passe vérifier les logs:

```
$ tail -f /tmp/centos-setup.log
```

